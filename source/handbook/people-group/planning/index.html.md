---
layout: markdown_page
title: "People Group Planning"
---

People Operations Specialists will manage a monthly People Group calendar to highlight important dates.  Examples would include Summit dates, experience factor worksheet dates, merit or compensation program dates, manager and team member training, etc.  

## 2019 People Operations Planning
- **January 1st** - Annual compensation adjustments effective
- **January 1st** - Exchange Rate Review
- **January 16th** - 360 kick off via Cultureamp
- **January** - Manager Training (TBD)
- **February** - Manager Training (TBD)
- **March** - Manager Training (TBD)
- **April** - Manager Training (TBD)
- **May** - Manager Training (TBD)
- **May** - Summit
- **June** - Manager Training (TBD)
- **July 1st** - Exchange Rate Review
- **July 17th** - 360 Kick off via Cultureamp
- **July** - Manager Training (TBD)
- **August** - Manager Training (TBD)
- **September** - Manager Training (TBD)
- **August/September** - Open Enrollement (TBD) 
- **October** - Experience Factor Worksheets and Annual Compensation Review (TBD) 
- **October 14th** - Engagement Survey via Cultureamp
- **October 16th** - Experience Factor Worksheet Training for Managers and Team members
- **November** - Annual Experience Factor Worksheet due to People ops analyst
- **November** - Manager Training (TBD)
- **December** - Manager Training (TBD)
