---
layout: handbook-page-toc
title: "GitLab BV (Netherlands) Benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-group/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to Netherlands based employees

## Vacation Money

Dutch employees get the customary month of vacation money (Vakantiegeld) in the month of May, as defined [by the government](https://www.rijksoverheid.nl/onderwerpen/vakantiedagen-en-vakantiegeld/vraag-en-antwoord/hoe-hoog-is-mijn-vakantiegeld).
Note that Vakantiegeld is built into offers as well as our [Compensation Calculator](/handbook/people-group/global-compensation/#compensation-calculator).

Note that you can choose to have your vacation money distributed throughout the year.
To request a change in the way you receive this, please reach out to peopleops@gitlab.com. [This template](https://docs.google.com/document/d/1IizjvTbZsAz1gCq5uYHlxhVvRW8LaEAlMrbHPHkgs1E/edit) should be completed by a member of the People Operations Specialist team, signed by GitLab and the team member and then send to HRSavvy to request the change.

## Medical

There is no additional medical coverage scheme through GitLab.

Note that in the Netherlands every citizen is obliged to have at least a basic health insurance package (basisverzekering) with a health insurance company. If you do not have health insurance, you can be fined by the Sociale Verzekeringsbank.

GitLab does not plan on adding additional medical cover at this time due to the governmental cover.

## Pension

State pension (AOW) is standard and will be paid out at retirement age. There is no additional pension scheme through GitLab.

GitLab is currently reviewing adding a private pension plan.

## 30% Tax Ruling

Eligible employees may apply for the 30% Tax Ruling. More information can be found on our [Visas](/handbook/people-group/visas/#30-ruling-in-the-netherlands) page. Kindly note, this can also be [expensed](/handbook/people-group/visas/#expensing).

## GitLab B.V. Netherlands Leave Policy

* Statutory Maternity leave
  - The employee is entitled to a maximum six weeks' leave prior to the estimated date of childbirth and for ten weeks after that date; therefore totaling sixteen weeks.
  - The employee can reduce the leave period prior to the estimated date of childbirth to at least four weeks. In that case, the number of days not taken prior to the estimated date of childbirth is added to the leave period following the estimated date of childbirth.
  - In the event of incapacity for work from six weeks prior to the estimated date of childbirth, the sixteen-week period for pregnancy and childbirth leave commences at that time, regardless of which agreements have been made.
  - Besides this, you’re also entitled to continuous wage during your leave which will still be paid out by GitLab. However, the [UWV](https://www.uwv.nl/overuwv/english/about-us-executive-board-organization/detail/about-us) offers GitLab a WAZO (Work and Care Act) settlement which is destined to cover your salary while on leave. The UWV ensures expert and efficient implementation of employee insurance and the WAZO settlement is one of these insurances. This maternity benefit (WAZO) lasts at least 16 weeks and covers 100% of the daily wage. In order for GitLab to receive this settlement HRSavvy will inform the UWV about your pregnancy via an application form. To apply, please inform peopleops@domain and compensation@domain when you’d wish for your maternity leave to start. The exact start date is up to you to decide. Please note that your leave can start 6 weeks prior to, but no later than, 4 weeks before your due date. HRSavvy will then work with you directly to apply and keep peopleops@domain in cc.

* Statutory Paternity Leave
  - After your partner has given birth you are entitled to up to five days of paid paternity leave.

* Statutory Parental leave
  - Employees who have children under the age of eight are entitled to take unpaid parental leave. In the case of a family with more than one child under the age of eight, that right is applicable for each child. The number of hours' leave is thirteen times the weekly working hours (65 days for full-time employment). However, no more than half of the number of weekly working hours can be taken each week.

People Ops will consult with HRSavvy to ensure that the statute is met.

### Applying for Leave in the Netherlands

HRSavvy can assist in applying for maternity leave covered by social security. In this application the company can decide whether the benefit is paid to the employee directly, or the employer continues paying the salary and receives the benefit. The last option is done in most of the cases.
