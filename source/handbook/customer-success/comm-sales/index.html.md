---
layout: handbook-page-toc
title: "Commercial Sales - Customer Success"
---

# Commercial Sales - Customer Success Handbook
{:.no_toc}

GitLab defines Commercial Sales as worldwide sales for the mid-market and small/medium business segments. [Sales segmentation](/handbook/business-ops/resources/#segmentation) is defined by the total employee count of the global account. The Commercial Sales segment consists of two sales teams, Small Business (SMB) and Mid-Market (MM). The Commercial Sales segment is supported by a dedicated team of Solutions Architects (SA) and Technical Account Managers (TAM).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Role & Responsibilities

### Solutions Architects

Solutions Architects are aligned to the Commercial Sales Account Executives by a pooled model. Requests for an SA will be pulled from the triage board by an SA based on multiple factors including availability, applicable subject matter expertise, and workload.  

* [Solutions Architect role description](/job-families/sales/solutions-architect/)
* [Solutions Architect overview](/handbook/customer-success/solutions-architects/)
* [When and how to engage a Commercial Solutions Architect](/handbook/customer-success/solutions-architects/#commercial-engagement-model)
* [How incoming SA requests are triaged](/handbook/customer-success/solutions-architects/#triage-of-issues)

### Technical Account Managers

Technical Account Managers that support Commercial Sales are aligned by region, Americas East, Americas West, EMEA and APAC. Not all accounts will have a dedicated TAM. Account qualification is required.

* [Technical Account Manager role description](/job-families/sales/technical-account-manager/)
* [Technical Account Manager overview](/handbook/customer-success/tam/)
* [When and how a TAM is engaged](/handbook/customer-success/tam/engagement/)

## Seamless Customer Journey

A seamless customer journey requires a continuous flow of relevant information between the roles at GitLab with customer outcomes in focus. Below are some examples of the transfer of information between roles that may be required.

### TAM to TAM (existing accounts)

* Ensure all accounts have a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Update TAM name on account team in Salesforce
* Share any Outreach sequences or templates currently in use
* The new TAM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)
* Evaluate the state of the account's license including an assessment of active users and if a true-up is necessary
* Evaluate the state of tickets to ensure any open critical issues are addressed
* Retrieve any architectural diagrams, machine specifications, and gitlab configuration files to assess current environment

### Account Executive to TAM (existing accounts without a TAM)

* SA or TAM creates a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Add TAM name to account team in Salesforce
* Identify any relevant Outreach sequences or templates
* The new TAM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)
* Review the [Account Handoff Checklist](/handbook/customer-success/comm-sales/account-hand-off-tam-checklist.html) for additional info

### SA to TAM (new accounts)

* SA creates a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Ensure any account notes held outside the project are linked to Salesforce and shared with the TAM
* SA to clearly outline to TAM how far the customer is in their adoption of GitLab
* TAM begins Outreach sequence for new customers
* Add TAM name to account team in Salesforce, ensure SA name is already present
* Ensure any urgent action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)

### SA to SA (new accounts)

* Update SA name on account team in Salesforce
* Ensure any account notes are linked to Salesforce and shared with the new SA
* Introduce new SA live on a client call
* If a [POC](/handbook/sales/POC/) is pending or active, update the POC record in Salesforce as required
* Ensure any current action items are identified via an issue on the [Commercial SA Triage board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/boards/1006966)

## Customer Engagement Guidelines

### Ongoing Customer Communication

For commercial accounts, we do not currently offer our customers Slack channels unless they meet specific criteria and it would be highly beneficial to both the customer and GitLab teams. Prior to creating the channel and provisioning access, the TAM for the account must approve its creation and use. To qualify, the customer should be at minimum:
- Premium/Silver or above
- 200k+ IACV

[Collaborative projects](/handbook/customer-success/tam/engagement/#managing-the-customer-engagement) should be suggested first with few possible exceptions (such as reference customers, early adopters or strategic partnerships).  If the customer is evaluating GitLab or doing a POC for an upgrade, SAs or TAMs can request an ephemeral Slack channel, but the channel should be closed within 90 days if it does not meet the criteria shown above. Slack is simply too intensive to scale for Mid-Market and SMB.

Meanwhile, it is not scalable for every customer to have a collaboration project, and the CS team will determine the need for a project on a per-customer basis.

## Sales Engagement Guidelines

### Mid-Market Presales Customer Engagement

To request SA assistance, create an issue on the [Americas Commercial SA Triage board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/boards/1006966) or the [EMEA Commercial SA Triage board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/emea-commercial-triage/-/boards) using the relevant issue template. For more detail on creating an issue, please reference the [Solutions Architect Handbook page](/handbook/customer-success/solutions-architects/#commercial-engagement-model). 

Typically, once an SA pulls the work from the Triage board, that SA will consistently service that customer after that first meeting. Additional triage requests should not be required for follow-on meetings or other customer outreach so as to create a consistent customer experience via a singular technical contact. SA's may always call on each other for assistance as needed. Typical conditions where other SA's will get involved in an account include:

* Availability: The SA servicing the customer is unavailable due to PTO or other scheduling conflict
* Expertise: Another SA has notably deeper subject matter expertise and would create a better customer experience for a specific topic
* Workload: If a series of calls turn into multiple POC's, security audits or other high engagement activity, then the SA team may have to rebalance the workload to provide adequate service level and response time for prospects and customers

Once a customer enters or completes a technical evaluation of GitLab, it is the responsibility of the Account Executive to transition the opportunity stage within Salesforce from Stage 3 to the next logical stage. It is the responsibility of the SA to ensure that Stage 3 transition data (such as win/loss reasons) have been properly entered, the migration from stage 3 has happened in a timely manner once SA activity is complete, and any POC data and status is correct in Salesforce.

#### When to Engage

As a best practice, an SA should not attend discovery calls as it does not lend itself to scaling the SA team efficiently. However, there are times when SA attendance on a discovery call is the right activity. This will be left to the discretion of the Account Executive, but may include reasons such as:

* The Account Executive is newer to GitLab and the SA would help improve customer experience on early calls
* An existing customer with specific needs hints at a technical deep dive early on in an upgrade conversation
* The opportunity is large and a full account team introduction will help set the tone of the customer journey
* The customer is interested in Professional Services and an SOW may be required

#### Additional Guidelines

Avoid direct messaging an SA for general questions. Direct messages between an SA and an Account Executive should only happen when the two individuals are actively involved in an engagement that initiated from the SA triage board, and the message context is related to that engagement. Other questions should be posted in the #cs_commercial Slack channel.

Avoid last-minute meeting requests. SA's cannot commit to these with any consistency. If a customer does request a near-term meeting, create an issue on either the Americas or EMEA Commercial SA Triage board and also post a link to the issue in the #cs_commercial channel. The SA's will do their best to accommodate as schedules allow. This is not a best practice as it does not allow the SA sufficient prep time for customer context and frequently results in a sub-optimal experience for the customer.

SA's help with guided POC's, but as a general guideline they do not provide real-time help for free trials. For product evaluations with IACV greater than $10K USD, any existing trials should be converted to a guided a Proof of Concept so the SA can better assist the customer. In the case where Mid-Market customers are executing a free trial and are seeking technical assistance, it is recommended to obtain as much information as possible and post an issue for asynchronous assistance via the SA Triage boards.

### SMB Presales Customer Engagement

Since the SMB team does not currently have dedicated Solutions Architects, the SMB team may engage the Mid-Market CS team for pre-sales or post-sales support using the following guidelines:

#### Asynchronous Support

SMB Customer Advocates may request asynchronous technical pre-sales assistance for any SMB prospect using the Commercial SA Triage Board via [the process](/handbook/customer-success/solutions-architects/#commercial-engagement-model) documented on the Solutions Architects page, or they can request asynchronous technical post-sales assistance for current SMB customers using the [Commercial TAM Triage Board](https://gitlab.com/gitlab-com/account-management/commercial/triage).

The SMB Customer Advocate should obtain as much detail as possible and post the technical questions from the client into an issue on the relevant Commercial Triage board.  

The relevant Mid-Market CS team will respond in the issue with answers within 48 hours unless otherwise noted by the CS team.

#### Synchronous Support

SMB Customer Advocates may request synchronous technical assistance when opportunity threshold criteria are met. The request for real-time pre-sales assistance should utilize the Commercial SA Triage Board via [the process](/handbook/customer-success/solutions-architects/#commercial-engagement-model) documented on the Solutions Architects page; for real-time post-sales assistance, the SMB Customer Advocates should utilize the [Commercial TAM Triage Board](https://gitlab.com/gitlab-com/account-management/commercial/triage). If the prospect does not meet the criteria below, the request will be handled asynchronously, as outlined above, but SMB Customer Advocates may still inquire about surplus bandwidth and/or availability of the Mid-Market CS team using the #cs-commercial channel in Slack.

Criteria for Synchronous Engagement:  
  * Minimum engagement threshold is $10K IACV
  * Preference will be given to Ultimate/Gold or Premium/Silver when bandwidth is limited

Engagement Limitations:
  * No more than 2 synchronous pre-sales engagements (60 minutes total) may occur per account
  * No more than 2 synchronous post-sales engagements (60 minutes total) may occur per account per year
  * No guided Proof of Concept or dedicated trial guidance is available
  * No guided architecture or deployment assistance is available
