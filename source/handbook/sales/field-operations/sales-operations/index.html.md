---
layout: handbook-page-toc
title: "Sales Operations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## How to Communicate with Us

Slack: [#sales-support](https://gitlab.slack.com/messages/CNLBL40H4)  
Salesforce: [@sales-Support](https://gitlab.my.salesforce.com/_ui/core/chatter/groups/GroupProfilePage?g=0F94M000000fy2K)

## Charter

Sales Operations is a part of Field Operations. We aim to help facilitate new and existing processes throughout our field organization via the use of systems, policies, and direct support. Sales Operations is responsible for the following key areas:

*  Territories
*  Go To Market data
*  Bookings
*  Deal Support (see Deal Desk)
*  Sales Support
*  Partner Operations
*  Customer Success Operations

## Sales Support - Deal Desk and Sales Operations

The GitLab Sales Support team includes two groups: Deal Desk and Sales Operations.

The Deal Desk team is comprised of in-field resources aligned to the time zones of our sales team.

*  East and West coast analysts for Americas and APAC support
*  EMEA analysts for EMEA and APAC support

## SLAs

First touch within 4 business hours
Resolution or escalation within 24 business hours

## Typical Inquiries to Sales Support May Include

*  Pending Quote Approvals
*  Deal Desk Pending Approvals (opportunities to review or approve as Closed Won)
*  Quote generation
*  Ramp deals
*  Contract resets (credit opportunity and upgrade/add-on opp)
*  Quote assistance on existing quote objects
*  IACV calculation
*  Renewal ACV calculation
*  Vendor set-up requests
*  Salesforce Reporting
*  Salesforce Dashboards
*  Questions regarding territory alignment and ownership
*  Billing questions or support
*  Legal questions or support

## Flat Renewal Support

### Flat Renewal Support - Which Opportunities Qualify

* Renewal ACV is under $10,000
* Flat renewal - no upside or downside IACV (same value year over year)
* Renewal has no true-up
* Customer is in good standing (no billing disputes or collection issues)
* Billing contact and address is up to date
* No customer interaction is required

### How to Request Flat Renewal Support

1. Go to an Opportunity which meets the renewal support criteria
2. Click on the `Flat Renewal Support` button

### What Happens When Flat Renewal Support is Requested

1. A case is created within Salesforce
2. Deal Desk team is notified of case creation
3. If the request does not meet flat renewal support criteria, the Deal Desk team  will notify the requestor on the Opportunity chatter 
4. Request will be processed according to the [sales order processing guidelines](/handbook/business-ops/order-processing/#renew-existing-subscription-for-the-billing-account)
5. Deal Desk team will chatter on the opportunity when the renewal is processed

## Updating Zuora and Salesforce Quote Templates  
In order to update quote templates that are used in Salesforce, and pulled in from Zuora, please reference the below resources provided by Zuora. 
1.  [General overview to update quote templates](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates)
2.  [Leveraging mail merge fields to update templates](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/C_Customize_Quote_Template_using_Word_Mail_Merge) - This must be completed in Microsoft word and saved accordingly
3.  [Reference the merge fields that are supported](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/K_Supported_Merge_Fields_for_Quote_Templates_and_Mapping_to_Salesforce#Charge_Summary.Quote_Rate_Plan_Merge_Fields)
4.  [How to displaty multiple quote charges in a table](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/E_Customize_Quote_Templates_Using_Microsoft_Word_Mail_Merge)
5.  [Uploading to Zuora and connect to Salesforce](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings)

