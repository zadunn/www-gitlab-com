<!-- SET THE RIGHT LABELS AND MILESTONE (let the autocomplete guide you) -->

/label ~"release post" ~"Technical Writing" ~"devops::
/milestone %"
/assign `@PM`

Engineer(s): `@engineers` | Product Marketing: `@PMM` | Tech Writer: `@techwriter`

Please review the guidelines for feature block creation at https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-blocks.
They are frequently updated, and everyone should make sure they are aware of the current standards (PM, PMM and TW).

### PM release post item checklist

- [ ] Structure
  - Screenshot/video is included (optional for secondary items).
  - Check that [image size < 150KB](https://about.gitlab.com/handbook/marketing/blog/release-posts/#images).
  - Check if the image shadow is applied correctly. Add `image_noshadow: true` when an image already has a shadow.
  - Remove any remaining instructions (comments).
  - Feature is added to [`data/features.yml`](https://about.gitlab.com/handbook/marketing/website/#adding-features-to-webpages) (with accompanying screenshots).
- [ ] Frontmatter
  - Check feature availability frontmatter (`available_in:`) is correct: (Core, Starter, Premium, Ultimate).
  - Ensure videos and iframes added within the feature description are wrapped in `<figure class="video_container">` tags (for responsiveness).
  - Check documentation link points to the latest docs (`documentation_link:`), and includes the anchor to the relevant section on the page if possible.
  - Check that all links to `about.gitlab.com` content are relative URLs.
- [ ] Content
  - Description is informative and focuses on the problem we're solving and the value we're delivering. Be sure to make clear if it is a new feature, or an improvement to an existing feature.
  - Check title is in sentence case, and feature and product names are in capital case.
  - Check that documentation is updated, very clearly talks about the feature (mentions it by name).
  - Run the content through an automated spelling and grammar check.
  - Validate all links are functional and have [meaningful text](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) for SEO (e.g., "click here" is bad link text).

### Review

When the above is complete and the content is ready for review, it should be reviewed by Product Marketing and Tech Writing.
Please assign the PMM and Tech writer to the MR when it is ready to be reviewed!

- [ ] Tech writer and PMM assigned for review
  - [ ] Tech writer reviewed and approved
  - [ ] PMM reviewed and approved

#### PMM review

- [ ] PMM review
  - Make sure feature description is positive and cheerful.

#### Tech writing review

- [ ] Verify Frontmatter:
  - Check that all links and URLs are wrapped in single quotes `'` (`documentation_link: 'https://docs.gitlab.com/ee/#amazing'`).
  - Check that name fields are wrapped in double quotes `"` (`name: "Lorem ipsum"`).
  - No useless whitespace (end of line spaces, double spaces, extra blank lines, and lines with only spaces).
- [ ] Links
  - Verify all links (including in feature description) work and that anchors are valid (note: the H1 (top) anchor on a docs page is not valid). Links should not redirect.
  - Verify that the feature is clearly mentioned in the linked documentation. If not, suggest a better doc link. If none exists, note this and raise an issue for creation of documentation if needed.
- [ ] Content
  - Check that there are no typos or grammar mistakes, but style is up to the PM and PMM.
  - Ignoring style, verify that content accurately describes the feature based on your understanding of it.

### Links

- Feature Issue:
- Feature MR (optional):
- Release post (optional):
