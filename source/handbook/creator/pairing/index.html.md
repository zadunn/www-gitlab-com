---
layout: markdown_page
title: "Creator pairing"
---

## Introduction

Creator pairing is a program that allows any GitLab engineer to work together with GitLab co-founder Dmitriy Zaporozhets (DZ) from the same physical location for a week. 

## Goal

The goal of the program is to help DZ during his work as a fellow of initial delight. 
As an additional benefit, a co-worker will learn about how DZ thinks and sees the product.

## Things to Know

* You need to travel to DZ location. 
* The duration is one week per person. 
* Don't plan to do any of your usual work. Tell your team you're on paid time off.
* The company will pay for travel and hotel.

## Who can apply

Any [senior or higher](/handbook/engineering/career-development/#engineering) backend or frontend engineer at GitLab.

## How to apply

1. Create a merge request to add yourself to the [schedule](#schedule). 
1. Ask your manager to approve (but not merge) the merge request. 
1. Assign the merge request to DZ (`@dzaporozhets`).

## Schedule

| Start date | End date | Who |
|---|---|---|
| 2019-11-04 | 2019-11-08 | [Tetiana Chupryna ](https://gitlab.com/brytannia)
| 2019-11-11 | 2019-11-15 | [Illya Klymov](https://gitlab.com/xanf)
| 2020-01-27 | 2020-01-31 | [Imre Farkas](https://gitlab.com/ifarkas)
