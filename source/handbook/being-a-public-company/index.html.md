---
layout: handbook-page-toc
title: "Being a public company"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## Being a public company

1. As detailed in our [company strategy](/company/strategy/), GitLab wants to go public in CY2020, specifically on Wednesday, November 18, 2020.
1. Going public is like graduating from high school, a great day but it shouldn't be the biggest thing you achieve in life.
1. We have a [strategy with clear goals for November 18, 2023](/company/strategy/#sequence) such as getting to $500m ARR.
1. About stock prices, the father of value investing, Benjamin Graham, [explained this concept](https://news.morningstar.com/classroom2/course.asp?docId=142901&page=7) by saying that: "In the short run, the market is like a voting machine, tallying up which firms are popular and unpopular. But in the long run, the market is like a weighing machine, assessing the substance of a company." The message is: What matters in the long run is a company's actual underlying business performance so we should focus on our [KPIs](/handbook/ceo/kpis/) and particularly on growing our [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv).
1. When you bring up the stock price in a work conversation the response should be: lets not get distracted by the stock price we can't control in the short term and focus on growing IACV instead.
1. We can't control our stock price nor the volatility of that price in the short-term, of the two the volatility compared to the rest of the market is something we can reduce by realizing our plans, meeting expectations, and making sure investors understand our financial model, priorities, expectations, forecast and strategy.
1. We will make everyone an insider, this means your trading is restricted but we can share financial information inside the company.
1. We never want to go back in transparency. For example we never released financial information publicly because as a public company you can only do that when it is audited.
1. We should evaluate spending proposals with Return On Investment (ROI) calculations, industry benchmarks, and the most efficient way to do things. Never compare the amount spend to our company valuation, revenue, or assets.

This statement is being made pursuant to, and in accordance with, Rule 135 under the Securities Act of 1933, as amended (the “Securities Act”) and shall not constitute an offer to sell, or the solicitation of an offer to buy, any securities. Any offers, solicitations or offers to buy, or any sales of securities will be made in accordance with the registration requirements of the Securities Act.

## Will GitLab be acquired?

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/LIXLGyZK72c" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, GitLab co-founder and CEO [Sid Sijbrandij](https://gitlab.com/sytses) discusses the topic of remaining independent as a company with [Kristóf Éger](https://gitlab.com/keger). The [coffee chat](/company/culture/all-remote/informal-communication/#coffee-chats) is transcribed, in part, below.*

> Our intention, from the moment we took external funding, was to stay independent. 
> 
> The reason we want to stay independent is we think it will better allow us to preserve our culture — we have our [six values](/handbook/values/) that are important to us — and also to be a good steward of open source. 
>
> Now, we're not totally in control of that. The majority of GitLab is owned by venture capitalists. But, we do have some sway. If, as an executive team, you're not interested in being acquired, it's harder to acquire a company. 
>
> We're always more [optimistic](/handbook/values/#focus-on-improvement) about the future than anybody outside of the company. That means that we have to keep growing. We have to keep growing [IACV](/handbook/sales/#incremental-annual-contract-value-iacv), and keep growing our revenue. 
>
> We also have to keep growing as a product. We're investing a whole lot of money in developing GitLab further. That shows up as a big expense, so that makes the company less attractive. Longer-term — if we build the right things, the right way — that makes the company more attractive.
>
> We keep investing in the future in order to not get bought. We've been very clear with our investors what our intentions were from the start. We're doing everything we can to stay independent. - *GitLab co-founder and CEO Sid Sijbrandij* 
