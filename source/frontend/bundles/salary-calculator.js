import Vue from 'vue';
import SalaryCalculator from "../components/salary-calculator/SalaryCalculator.vue";

document.addEventListener(
  'DOMContentLoaded',
  () => {

    const calculators = document.querySelectorAll('.salary-calculator');

    calculators.forEach((el) => {
      new Vue({
        el: el,
        components: {
          SalaryCalculator,
        },
        render(createElement) {
          return createElement(SalaryCalculator, {
            props: {
              initialRole: el.dataset.role
            }
          });
        },
      })
    });
  }
);
