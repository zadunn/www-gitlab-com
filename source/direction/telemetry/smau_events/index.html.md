---
layout: markdown_page
title: "Category Vision - SMAU Events"
---

- TOC
{:toc}

Stage Monthly Active Users (SMAU) is a Product KPI designed to capture the number of active users per stage. The definition of what constitutes an Active User is different for each stage. A broader definition of an Active User could be "any user who performs, during a given time period, a meaningful event directly leading to long-term engagement and retention".

At the moment, we haven't conducted any data analysis to help Stage PMs define what their stage's ideal Active User definition should be. PMs have used their empirical knowledge about their stage to define what actions would make a user become "active".

### Implementation

* In Q3 2019, we started creating SMAU dashboards for each of the product stages using Snowplow page view data. Because Snowplow is currently only implemented on GiLab.com, our SMAU reporting is currently limited to GitLab.com usage.
* Our Snowplow event tracking doesn't currently send any user_id information. Because of this, we are unable to reconcile our frontend events with our backend events and cannot provide a true "SMAU" number for a stage without massively over-reporting. In lieu of user_id for frontend events, we are using a browser-based [domain_userid](https://github.com/snowplow/snowplow/wiki/snowplow-tracker-protocol#15-user-related-parameters) but we make no attempt to reconcile frontend and backend events together.

### Stage Summary Table

|Stages   |V1 (page view) Status|Periscope Link                                                              |Original Definition Issue                        |
|---------|---------------------|----------------------------------------------------------------------------|-------------------------------------------------|
|Manage   |Completed            |https://app.periscopedata.com/app/gitlab/516500/Manage-GitLab.com-SMAU      |https://gitlab.com/gitlab-org/telemetry/issues/47|
|Plan     |Completed            |https://app.periscopedata.com/app/gitlab/514288/Plan-GitLab.com-SMAU        |https://gitlab.com/gitlab-org/telemetry/issues/48|
|Create   |Completed            |https://app.periscopedata.com/app/gitlab/514285/Create-GitLab.com-SMAU      |https://gitlab.com/gitlab-org/telemetry/issues/49|
|Verify   |Completed            |https://app.periscopedata.com/app/gitlab/522860/Verify-GitLab.com-SMAU      |https://gitlab.com/gitlab-org/telemetry/issues/50|
|Package  |Completed            |https://app.periscopedata.com/app/gitlab/527857/Package-GitLab.com-SMAU     |https://gitlab.com/gitlab-org/telemetry/issues/51|
|Secure   |Not Defined          |                                                                            |https://gitlab.com/gitlab-org/telemetry/issues/55|
|Configure|In-Progress          |In-Progress                                                                 |https://gitlab.com/gitlab-org/telemetry/issues/53|
|Release  |Not Defined          |                                                                            |https://gitlab.com/gitlab-org/telemetry/issues/52|
|Monitor  |Completed            |https://app.periscopedata.com/app/gitlab/522840/Monitor-GitLab.com-SMAU     |https://gitlab.com/gitlab-org/telemetry/issues/54|
|Defend   |Not Defined          |                                                                            |https://gitlab.com/gitlab-org/telemetry/issues/56|




### Events Summary Table
This table summarises for each stage, which events are **currently** taken into consideration for our SMAU calculations.

**Column Definitions:**  
* Stage: one of the stages defined here. All specific events defined belong to exactly one stage.
* Event Name: Name of the event as defined by the Data Team. That is how it will show in Periscope in all SMAU related dashboards and Charts
* Description: description of the event.
* Event Type: An event can be:
   * a Frontend event e.g. an event performed by a user that interacts with a specific page or element of a page.
   * a Backend even e.g. an event directly tracked from a backend database. For example, a commit pushed to a specific remote branch could be only tracked directly from our backend.
* Frontend Action: A Frontend event could be either a page view or a specific interaction with some elements of a page (drag and drop an issue from a board page for example).  

| Stage   | Event Name                   | Event Description                       | Event Type | Frontend Action Type |
|---------|------------------------------|-----------------------------------------|------------|----------------------|
| Create  | mr_viewed                    | Specific MR page viewed                 | Frontend   | Pageview             |
| Create  | project_file_viewed_in_ide   | Projet file viewed in IDE               | Frontend   | Pageview             |
| Create  | repo_file_viewed             | Repo file viewed                        | Frontend   | Pageview             |
| Create  | search_performed             | Search Performed                        | Frontend   | Pageview             |
| Create  | snippet_created              | New snippet created                     | Frontend   | Pageview             |
| Create  | snippet_edited               | Snippet edited                          | Frontend   | Pageview             |
| Create  | snippet_viewed               | Snippet page viewed                     | Frontend   | Pageview             |
| Create  | wiki_page_viewed             | Wiki page viewed                        | Frontend   | Pageview             |
| Create  | mr_created                   | Merge request is created                | Backend    |                      |
| Create  | mr_comment_added             | Merge request comment is addded         | Backend    |                      |
| Create  | snippet_comment_added        | Snippet comment is added                | Backend    |                      |
| Manage  | audit_events_viewed          | Audit Events log viewed                 | Frontend   | Pageview             |
| Manage  | code_analytics_viewed        | Code Analytics is viewed                | Frontend   | Pageview             |
| Manage  | convdev_index_viewed         | ConvDev Index is viewed                 | Frontend   | Pageview             |
| Manage  | cycle_analytics_viewed       | Cycle analytics for a project is viewed | Frontend   | Pageview             |
| Manage  | group_analytics_viewed       | Group Analytics is viewed               | Frontend   | Pageview             |
| Manage  | group_created                | /groups/new page is viewed              | Frontend   | Pageview             |
| Manage  | project_created              | A new project is created                | Backend    |                      |
| Manage  | user_created                 | A new user account is created.          | Backend    |                      |
| Manage  | user_authenticated           | A user successfully logs in             | Frontend   | Pageview             |
| Monitor | environments_viewed          | Environment Page is viewed              | Frontend   | Pageview             |
| Monitor | error_tracking_viewed        | Error tracking Page is viewed           | Frontend   | Pageview             |
| Monitor | logging_viewed               | Logs page is viewed                     | Frontend   | Pageview             |
| Monitor | metrics_viewed               | Metrics Page is viewed                  | Frontend   | Pageview             |
| Monitor | operations_settings_viewed   | Operation Settings Page is viewed       | Frontend   | Pageview             |
| Monitor | prometheus_edited            | Prometheus Service is edited            | Frontend   | Pageview             |
| Monitor | tracing_viewed               | Tracing Page is viewed                  | Frontend   | Pageview             |
| Package | container_registry_viewed    | Container Registry page is viewed       | Frontend   | Pageview             |
| Package | dependency_proxy_page_viewed | Dependency Proxy page is viewed         | Frontend   | Pageview             |
| Package | packages_page_viewed         | Package is viewed                       | Frontend   | Pageview             |
| Plan    | board_viewed                 | Board Page Viewed                       | Frontend   | Pageview             |
| Plan    | epic_list_viewed             | Epic List Page viewed                   | Frontend   | Pageview             |
| Plan    | epic_viewed                  | Epic Page viewed                        | Frontend   | Pageview             |
| Plan    | issue_list_viewed            | Issue List page viewed                  | Frontend   | Pageview             |
| Plan    | issue_viewed                 | Issue Viewed                            | Frontend   | Pageview             |
| Plan    | labels_viewed                | Label Page Viewed                       | Frontend   | Pageview             |
| Plan    | milestones_list_viewed       | Milestone list viewed                   | Frontend   | Pageview             |
| Plan    | milestones_viewed            | Mileston viewed                         | Frontend   | Pageview             |
| Plan    | notification_settings_viewed | Notification Settings Viewed            | Frontend   | Pageview             |
| Plan    | personal_issues_viewed       | Personal Issues Viewed                  | Frontend   | Pageview             |
| Plan    | roadmap_viewed               | Roadmap Viewed                          | Frontend   | Pageview             |
| Plan    | todo_viewed                  | To do page viewed                       | Frontend   | Pageview             |
| Verify  | gitlab_ci_yaml_edited        | gitlab ci YAML file viewed              | Frontend   | Pageview             |
| Verify  | gitlab_ci_yaml_viewed        | gitlab ci YAML file edited              | Frontend   | Pageview             |
| Verify  | job_list_viewed              | job list Page viewed                    | Frontend   | Pageview             |
| Verify  | job_viewed                   | specific job page viewed                | Frontend   | Pageview             |
| Verify  | pipeline_charts_viewed       | Pipeline Charts viewed                  | Frontend   | Pageview             |
| Verify  | pipeline_list_viewed         | Pipeline List page viewed               | Frontend   | Pageview             |
| Verify  | pipeline_schedules_viewed    | Pipeline schedules page viewed          | Frontend   | Pageview             |
| Verify  | pipeline_viewed              | Specifix Pipeline page viewed           | Frontend   | Pageview             |
