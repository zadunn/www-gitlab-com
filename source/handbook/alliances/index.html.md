---
layout: handbook-page-toc
title: "GitLab Alliances Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Alliances Mission

**Accelerate GitLab’s trajectory by connecting the great work every team is doing with the outside world**

## Alliances KPI's

### Active installations per hosting platform
Total active installations per hosting platform (Total, AWS, Azure, GCP, IBM, Red Hat, Digital Ocean, etc) in a given calendar month.

### Active SMAU
Current [SMAU](/handbook/product/growth/) per hosting platform (Total, AWS, Azure, GCP, Red Hat, Digital Ocean, Unknown etc) in a given calendar month. 

### Product Download by distribution method 
Total downloads by installation method (Omnibus, Cloud native helm chart, Source, etc).
<embed width="100%" height="100%" src="https://www.periscopedata.com/api/embedded_dashboard?data=%7B%22chart%22%3A+6924248%2C+%22dashboard%22%3A+446004%2C+%22embed%22%3A+%22v2%22%2C+%22border%22%3A+%22off%22%7D&signature=f0d97ebf40bb5a4b910805dca0a394dbee604d41c2aff4ad7d9ab608ccc523c7">



## Open to collaboration

GitLab is an open product with a Core, open-source edition, and a enterprise, open-core edition, with additional features that are closed-sourced.

We're open to integrations with companies and tools that are interested in harnessing GitLab's platform capabilities, user flows and data with their products.

Interested in a GitLab Technology Partnership? 
If you want to talk to us about a partnership please create a shared Google Doc that includes: details about the technical integration, end user benefits and business relationship that you would ideally want. Then please contact the [Alliances](mailto:alliance@gitlab.com) team.

## Partner definitions

### Technology partners

Primary monetization is through the sales of software licensing/support that integrates with GitLab
1. Platform Partners. These are critical partnerships and will be prioritized to support our customer demands.
    1. Cloud partners. For example AWS, Google, Azure, Digital Ocean, IBM
    1. Kubernetes partners: Red Hat, Rancher, VMWare, Mesosphere, Cisco, NetApp, etc.   
1. Integrated  Partners. Examples: Jenkins, Codeship, Shippable, TeamCity, Atlassian.  
    1. **Outbound**: There is a core set of priority outbound partnerships we will maintain and focus on. 
       Those are: [GitHub](/solutions/github/), BitBucket, [Jira](/solutions/jira/) and [Jenkins](/solutions/jenkins/)
    1. **Inbound**: GitLab [plays well with others](/handbook/product/#plays-well-with-others).
       This means we will support integrations to make them successful by enabling them within our product but we encourage other integration partners to [make and maintain the integration with us](/partners/integrate/).
       The work required to deliver the integration will be provided by the partner. 
       For go-to-market initiatives with inbound partners, we are open to collaboration but expect them to lead the effort. 
1. Sell-to Partners - Most of CNCF and majority of tech partners.
1. Upstream Partners. Examples: JetBrains, Visual Studio, Eclipse, Slack, Cloudability.

### Services partners

Primary monetization is through the sale of services.  This can be a one-time implementation, ongoing support, or outsourcing.
1. Global Systems Integrators - have a large global workforce and can deliver on almost any customer need. Examples: Accenture, Deloitte, TCS, Wipro
1. Regional Systems Integrators - large workforce but with single continental focus and a more limited offering of services. Examples: CI&T, Slalom
1. Boutique Systems Integrators - very focused DevOps partners that could be deep experts on GitLab and the nuances of getting it setup and running it. Examples - CloudReach, Flux7
1. Managed Service Providers - provide ongoing support for solutions/applications. Examples: Rackspace

### [Resellers](/resellers/)

Primary monetization is through reselling GitLab.
1. VAR/VAD (Value Added Reseller or Distributor) - Channel services including resale, implementation, contracting, support, financing etc.
1. DMR (Direct Market Reseller)-  primary business is resale of the software, often does not implement.  Value are the contracts that these partners have in place with customers.
1. Training Partners - focus on training companies, teams and often certifications

## Criteria for successful partnerships

1. Exposure - Partnerships which generate more exposure to new segments of audiences, integrations are a good example.
1. Product usage familiarity - more people using GitLab but not necessarily installing their own instance (GitLab.com), open source projects as an example.
1. Adoption - partnerships attracting more people to adopt GitLab for their own instance.  Upstream and downstream partners help here.
1. Revenue - Revenue generating partnerships.  Users come first but focus on revenue has ensured we have the revenue for sustainable growth.
1. Strategic - partners we find will add strategic value to our long term positioning and often around competitive situations.
1. Alignment - collaboration is stronger and our customers have a great experience when the Product, Marketing, and Sales teams are aligned. 

## GitLab delivery models
There are many ways that GitLab can be both installed (/install/) and once installed many deployment environments that GitLab can target.  Below is a structure to think about those options and some of the trade-offs that are made depending on the model.
* Self-managed - customer downloads, installs and maintains themselves
* GitLab Hosted - same code as self managed but maintained by a third party
* Marketplace - self-managed but purchased through marketplaces
* GLaaSTS - fully managed (by the cloud provider) private instance of GitLab.  Similar to the managed database options clouds offer.
* GitLab.com - fully managed multi tenent offer by GitLab Inc.

| Delivery Model | Self-Managed | GitLab Hosted | Marketplace | GLaaSTS | GitLab.com |
| -------------- | ------------ | ------------- | ----------- | ------- | ---------- |
| State | Active | Deprecated | Soon | Future | Active |
| Tenents | Single | Single | Single | Single | Multi |
| Installation Package | Omnibus/Helm | Omnibus/Helm | Omnibus/Helm | Helm | Moving to Helm |
| Managed | Self | Partner | Self | Cloud | GitLab |
| Billing Ownership | GitLab/Partner | Partner | GCP, AWS, DO, etc | Cloud | GitLab |
| Infrastructure Incl. | no | yes | yes | yes | yes |
| Partners | SI's/VAR's | MSP's | Clouds | Clouds | GitLab/Partner |
| Sales Focus | Hybrid/Multi-Cloud | Partner | Self-service | Self-service | Self-service |
| Pricing | user/yr | user/yr | user/month or hour | user/yr | user/yr |

## Acquisitions

If you are interested to inquire about a potential acquisition of your company please visit our [acquisition handbook](/handbook/acquisitions).

## Workflow guidelines

### Handling inbound alliance requests if at GitLab

If you've received an inbound alliance request please post a new message to the #alliances Slack channel with the brief description of the request. Once posted, the alliances team will declare who will take lead on that request and the necessary next steps to take.

### Google docs
1. Gdocs which are shared with the partners should be set to "Anyone with the link". Internal Gdocs should be set to "GitLab".
1. The following structure should be used for Gdoc names: "PARTNER NAME and GitLab".
1. Alliance folder contain current work with partners and are kept by Partner name

### Cloud images

See the [Cloud image process](./cloud-images/) page for guidelines.

### MDF cloud credit reimbursement process

When applying for MDF funds which are reimbursed in the form of cloud credits to a GitLab account follow the following process to make sure all relevant parties are informed:
1. Create issue with the following details on the campaign:
   1. Campaign topic
   1. Timeline
   1. Budget
   1. Marketing targets
1. Budget spend approval and acknowledgement from AWS/Google/etc
1. Future credit acknowledgement - by production team lead

### License requests

When partners and potential partners request free licenses to develop integrations with GitLab most need a self-managed license key. To generate a license follow the steps below according to the type of license.

#### Self-managed
   1. Inquire with the partner how many developer seats will be necessary.
   1. Go to [license.gitlab.com](https://license.gitlab.com/) and sign-in with your GitLab credentials.
   1. Choose the manual entry option.
   1. Select the Ultimate license (unless requested otherwise).
   1. Set the term for 6 months.
   1. Set the appropriate number of seats; if unknown set at 5. The total number of seats should not exceed 10.
   1. Provide any relevant details about the purpose and use of the license in the free form text box at the bottom of the page.

#### GitLab.com
   1. Inquire with the partner how many developer seats will be necessary.
   1. [Create an issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues) under the [dotcom-internal project](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal) and choose the `plan-change-request` template.
   1. Set the appropriate number of seats; if unknown set at 5. The total number of seats should not exceed 10.