---
layout: job_family_page
title: "IT Helpdesk Analyst"
---

## Responsibilities

* Triage all IT related questions as they arise
* Build a knowledge base of IT practices and pragmatic problem solving in the handbook
* Account management for password resets and lockout
* On call support for immediate software and hardware issues during local business hours
* Diagnose computer errors and provide technical support
* Troubleshoot software and hardware
* Support Weekly IT Onboarding Sessions for new Team Members
* Train end-users how to setup and use new technologies
* Provide technical support over the phone or Web
* Use specialized help desk support software to take control of end-users' computers to troubleshoot, diagnose and resolve complex issues


## Requirements

* Experience with Gsuite and an interest in its administration
* Some experience Automation & Scripting  Mac and Linux environment
* Hands-on experience supporting Mac and or Linux
* Tolerance for repetitive or manual tasks, with a mind for automating said tasks
* Some experience with SQL and Python
* Configure, build, test, and deploy
* Experience working with Git

## Performance Indicators (PI)

*  [Cycle Time for IT Support Issue Resolution](/handbook/business-ops/metrics/#cycle-time-for-it-support-issue-resolution)
*  [Support Issues and Access Request Closed per Employee](/handbook/business-ops/metrics/#support-tickets-and-access-request-closed-per-employee)
*  [Average Merge Request](/handbook/business-ops/metrics/#average-merge-request)
*  [Average Delivery Time of Laptop Machines < 21 days](/handbook/business-ops/metrics/#average-delivery-time-of-laptop-machines--21-days) 
*  [Customer Satisfaction Survey (CSAT)](/handbook/business-ops/metrics/#customer-satisfaction-survey-csat)


## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Next, candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with our Director of Business Operations
* Next, candidates will be invited to schedule a second interview with one of our Sys Admins
* Candidates will then be invited to schedule a third interview with one of our Executive Admins
* Finally, candidates may be asked to interview with our CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
