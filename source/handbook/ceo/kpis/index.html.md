---
layout: handbook-page-toc
title: "KPIs"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What are KPIs

Every department at GitLab has Key Performance Indicators (KPIs).
Avoid the term metric where we can be more explicit. Use KPI instead.
A department's KPIs are owned by the respective member of e-group. A department may have many performance indicators (PIs) they track and not all of them will be KPIs. KPIs should be a subset of PIs, and used to indicate the most important PIs to be surfaced to leadership.

The KPI definition should be in the most relevant part of the handbook which is organized by [function and results](/handbook/handbook-usage/#style-guide-and-information-architecture).
In the definition, it should mention what the canonical source is for this indicator.
Where there are formulas include those as well.
Goals related to KPIs should co-exist with the definition.
For example, "Wider community contributions per release" should be in the Community Relations part of the handbook and "Average days to hire" should be in the Recruiting part of the handbook.

## List of KPIs

The data team maintains [a list of GitLab KPIs and links to where they are defined](/handbook/business-ops/data-team/metrics/).

## Parts of a KPI

A KPI or metric consists of multiple things:

1. Definition: how we calculate it
1. Target: What we strive to be above, e.g. IACV has a target
1. Cap: What we strive to be below, e.g. Turnover has a cap
1. [Job family](/handbook/hiring/job-families/): link to job families with this as a performance indicator
1. Plan: what we have in our yearly plan
1. Commit: the most negative it will be
1. 50/50: the median estimate, 50% chance of being lower and higher
1. Best case: the most positive it will be
1. Forecast: what we use in our rolling 4 quarter forecast
1. Actual: what the number is

## What is public?

In the doc 'GitLab Metrics at IPO' are the KPIs that we may share publicly.
All KPIs have a public definition, goal, and job family links.
The actual performance an various estimates can be:

1. Live reported
2. Quarterly reported
3. Private
