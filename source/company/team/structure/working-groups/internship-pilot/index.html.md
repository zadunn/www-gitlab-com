---
layout: markdown_page
title: "Internship Pilot Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | October 2, 2019 |
| Target End Date | November 1, 2019 for Primary exit criteria, 30 November for Secondary criteria |
| Slack           | [#wg_internship-pilot](https://gitlab.slack.com/archives/CNLLV1NEN/) (only accessible from within the company) |
| Google Doc      | [Internship Working Group Agenda](https://docs.google.com/document/d/1KsdRtkRcF4EOpL2s0JDC-th8SB7nHstw8cfceLrzG-o/edit#) (only accessible from within the company) |
| Issue Board     | [Issue board](https://gitlab.com/groups/gitlab-com/-/boards/1360344?&label_name[]=wg-internship-pilot)             |

## Business Goal

Run a pilot internship program to determine feasability for future programs.

Overview:
1. Intention of hiring available candidates that perform well at the end of the internship
1. Limit duration (see exit criteria)
1. Open to anyone starting their career in tech (not just students)
1. Timeline:
    1. Design in Oct
    1. Recruiting in Nov
    1. Offers in Dec
    1. Internship start Summer 2020
1. No specific diversity targets, recruitement to follow current GitLab [equal employment opportunity](https://about.gitlab.com/handbook/hiring/#equal-employment-opportunity) practice.

## Exit Criteria (~61%)

Due to the need to move quickly the exit criteria is grouped in primary and secondary with the former needing to be completed to start engaging candidates.

### Primary (66%)
1. [Define budget](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5526) => `100%`
    1. Determine number of internship positions available ✅
    1. Determine compensation ✅
    1. Determine budget for co-location ✅
1. [Communication plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5527) => `20%`
    1. Choose the internship program name ✏️
    1. Define branding opportunities for all-remote company culture ✏️
    1. Define communication milestones ✏️
1. [Recruitement plan](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5528) => `75%`
    1. Define candidate qualification criteria ✅ 
    1. Create job family page ✅
    1. Identify sourcing targets and engagement plan ✅   
    1. Create Greenhouse vacancies ☑️
1. [Internship program structure](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5529) => `60%`
    1. Determine duration of the internship ✅
    1. Determine timing of the program ✅
    1. Decide on all-remote vs timezone alignment vs co-located cohort ✅
    1. Define the criteria for teams to be able to request an intern slot ️️✏️
    1. Define expected day-to-day activities for interns (skeleton outline) ✏️
1. [Interviewing](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5530) => `75%`
    1. Define interview process and technical assessments ️️✏️

### Secondary (55%)
1. [Onboarding/offboarding](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5531) => `10%`
    1. Determine onboarding process ✏️
    1. Determine offboarding communication to interns ️☑️
1. [Measurements of success](https://gitlab.com/gitlab-com/www-gitlab-com/issues/5532) => `100%`
    1. Determine criteria for considering the pilot a success ✅

 (✅ Done, ✏️ In-progress, ☑️ Still to be done)   

## Roles and Responsibilities

| Working Group Role    | Person                | Title                          |
|-----------------------|-----------------------|--------------------------------|
| Executive Sponsor     | Eric Johnson          | VP of Engineering              |
| Facilitator           | Jean du Plessis       | Frontend Engineering Manager   |
| Functional Lead       | Roos Takken           | People Business Partner, Engineering |
| Functional Lead       | Nick Nguyen           | Engineering Manager, Ecosystem |
| Member                | Tanya Pazitny         | Quality Engineering Manager    |
| Member                | Clement Ho            | Frontend Engineering Manager   |
| Member                | Phil Calder           | Growth Engineering Manager     |
| Member                | Liam McNally          | Interim Recruting Manager      |
| Member                | John Hope             | Engineering Manager, Plan      |

