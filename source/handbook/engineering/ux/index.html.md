---
layout: handbook-page-toc
title: "UX Department"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

We make a software product that is easy to use, enables everyone to contribute, and is built for a diverse, global community. To achieve this vision, the UX department actively works with the wider GitLab community to understand user behaviors, needs, and motivations. And as a skilled team of UX practitioners, we go far beyond the basics of UI design (that's only a part of what we do), proactively helping to shape the product experience through generative user research, strategic UX initiatives, and useful technical documentation.  

To meet our goals, the UX Department works cross functionally with internal teams and community contributors. We believe in iterative, valuable, and proactive improvements. When we get things wrong, we quickly work to make them right. We're passionate about the GitLab product, and we strive to become subject-matter experts both in our specific stage groups and across the whole product.

## More reading about UX

* [How we work](/handbook/engineering/ux/how-we-work/): Learn how we work within our department and with cross-functional partners.
* [Pajamas](/handbook/engineering/ux/pajamas-design-system/): See our Design System, also known as "Pajamas".
* [UX resources](/handbook/engineering/ux/ux-resources/): Helpful information and links.
* [UX Research](/handbook/engineering/ux/ux-research/): Learn about our UX research team and how they work.

## UX vision

Our goal is to make GitLab the most usable DevOps tool on the market, so that teams of all kinds can build better products, faster.

We don't support just a single user type&mdash;whether that's across roles, industries, or company size. Instead, we actively solicit feedback from the wider GitLab community to understand:

- How cross-functional product teams work together in a variety of industries and environments.
- When and how the needs of different roles converge and diverge.
- How users spend their time at work and what success means in their job.
- What's working well for them today and what they wish was easier (both in our tool and in other DevOps tools they may use).

To achieve a shared vision, we partner with Product Management on product discovery, proactively identify opportunities for UX improvements, measure our success through KPIs, and continuously optimize how we work&mdash;both within our department and with cross-functional partners.

### Holistic UX

Though we structure our work around individual stages of the product (Plan, Manage, Create, etc.), we can't separate look, feel and process from what a user is trying to accomplish. We must maintain a focus on what the user needs to get done and how to deliver that in the most effective manner possible, including how users flow from one stage of the product to another. Maintaining a holistic overview of the path a user may take allows us to see the possible twists and turns along the way. With this knowledge, we can optimize the user experience.

It is the responsibility of each [Product Designer](/job-families/engineering/product-designer/) and [UX Researcher](/job-families/engineering/ux-researcher/) to understand how users may flow in and out of their assigned stage group(s). This means that everyone must understand the broader product, how stage groups relate, and the work that is planned and in process outside of their own focus area(s).

### Easy UX

GitLab solves complex problems, but that doesn't mean our product experience needs to be complicated. We always strive to design solutions that are easy to use, but without oversimplifying the product.

We acknowledge we're building a product that must support skilled power users, while still making it [easy for less experienced users](https://imposter-syndrome.lol/posts/a-few-thoughts-on-ember/#the-issues-with-learning-ember) and [non-developers](/handbook/product/#enabling-collaboration) to learn. Within each stage group, the learning curve must be at least comparable to our best-in-class competitors, with clear, cohesive workflows, a highly usable interface, and comprehensive documentation.  

## Stage group UX vision

We're working to create a user experience that is seamless throughout the product. But, our organizational model is defined by [stage groups](/handbook/product/categories/) that work as cross-functional teams. That means we must be intentional about [outside-in thinking](http://customerthink.com/outside-in-vs-inside-out-thinking/), so that we deliberately (and effectively) connect the related workflows built by functional teams.  

For each stage group, the UX department is creating a UX strategy based on industry best practices and customer research. You can see the in-process strategy work here:

* Development
    * Manage: Coming soon! {::comment}[Manage](/handbook/engineering/ux/stage-group-ux-strategy/manage/){:/comment}
    * Plan: Coming soon! {::comment}[Plan](/handbook/engineering/ux/stage-group-ux-strategy/plan/){:/comment}
    * Create: Coming soon! {::comment}[Create](/handbook/engineering/ux/stage-group-ux-strategy/create/){:/comment}
* CI/CD
    * Verify: Coming soon! {::comment}[Verify](/handbook/engineering/ux/stage-group-ux-strategy/verify/){:/comment}
    * [Release UX](/handbook/engineering/ux/stage-group-ux-strategy/release/)
    * [Package UX](/handbook/engineering/ux/stage-group-ux-strategy/package/)
* Ops
    * Configure: Coming soon! {::comment}[Configure](/handbook/engineering/ux/stage-group-ux-strategy/configure/){:/comment}
    * Monitor: Coming soon! {::comment}[Monitor](/handbook/engineering/ux/stage-group-ux-strategy/monitor/){:/comment}
* Secure
    * [Secure UX](/handbook/engineering/ux/stage-group-ux-strategy/secure/)
* Defend
    * Defend UX: Coming soon! {::comment}[Defend](/handbook/engineering/ux/stage-group-ux-strategy/defend/){:/comment}
* [Growth](/handbook/engineering/ux/stage-group-ux-strategy/growth/)
* [Enablement](/handbook/engineering/ux/stage-group-ux-strategy/enablement/)

## UX principles

Our principles are that GitLab should be **[productive](#productive), [minimal](#minimal), and [human](#human)**. We want to [*design*](https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434) the most complete, uncomplicated, and adaptable DevOps tool on the market, enabling our users to spend more time adding business value in their own jobs.

#### Productive
*Yielding favorable or useful results; constructive: a productive suggestion.*

**User Experience is part of everything we do.**
* The default assumption is that user experience is part of every issue.

**Everyone at the company understands the role of UX and everyone contributes to the user experience.**
* We encourage Product Managers, Engineers, and the wider GitLab community to contribute to creating an exceptional user experience. [Everyone is a designer](https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434).
* Designers involve Product Managers, Engineers, and the wider community in problem discovery and definition.
* Designers, Product Managers, and Engineers are involved in UX research, backing solutions with data.

**Our solutions make users feel confident and efficient.**
* Use the simplest, most boring solution for a problem.
* Understand the user's goals and circumstances.
* Respect the importance of their work and avoid gimicky details.
* Provide sufficient feedback and direction to achieve user goals.
* Design holistic experiences and workflows.

#### Minimal
*Simplicity driving maximum effect.*

**Design is about iteration, so we continuously iterate on our processes, too.**
* We aggressively break down issues into the smallest effort that gets results.
* The design system will never be finished; it should always be evolving.
* Whenever possible, start in low-fidelity (wireframes, greyscale, etc.) to make sure we get it right before we go any further.

**Minimize distractions and clutter so users can focus.**
* Understand the user journey and goals.
* Create hierarchy and a sense of direction to guide users through the flow.
* Maintain a strong information architecture.
* Don't be afraid to remove things that don't contribute to the user flow.

#### Human
*Self-aware. Emotional, yet rational. Understandable and helpful.*

**Be Ambitious**
* Think big, but break it down into small steps.
* Don’t be afraid to fail. Permission to fail builds a culture of experimentation.
* Quirkiness is part of our DNA. We should embrace it in the right moments and contexts.

**Be Helpful**
* See the world through other people's eyes, and try to understand their experiences deeply and meaningfully.
* Don’t overload the user.
* Don’t change established user flows, if not really necessary.
* Assist the community in making an impact on our product.
* Steer the user in the right direction if they end up in a “bad” place (without blaming them), and recognize their efforts and accomplishments!

## Design culture
The culture of the design department is characterized by the following:
* We're independent, autonomous, and not hierarchical.
* We have a high level of commitment and reliability to our team.
* We have ownership over our work and are motivated to step up, take risks, and go beyond our roles.
* We don't ask for permission, and we believe that everyone can be a part of the design process.
* We're passionate, kind, honest, direct, transparent, inclusive, and unafraid to ask for help.
* We work asynchronously, we value iteration, and our designs are never complete.
* We're open to ideas and actively collaborate with other teams.
* We're willing to use whatever tools or mediums best communicate our design solutions.

## Personas

Existing personas are documented within the [handbook](/handbook/marketing/product-marketing/roles-personas/).

New personas or updates to existing personas can be added at any time.

Personas should be:

* Informed by research.
* Driven by job title or feature.
* Gender neutral.
* Use bullet points and avoid long narrative.
* Use the [Jobs To Be Done framework](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done)

## UX on social media

We encourage UXers to share UX designs and research insights on social media platforms such as Twitter and Dribbble.

#### Twitter

You can contribute design-related posts to our [@GitLab Twitter account](https://twitter.com/gitlab) by adding your tweet to our [UX Design Twitter spreadsheet][twitter-sheet].

1. Add a new row with your tweet message, a relevant link, and an optional photo.
1. Ensure that your tweet is no more than 280 characters. If you’re including a link, ensure you have enough characters, and consider using a [link shortening service](https://bitly.com/).
1. The UX Lead will check the spreadsheet at the beginning of each week and schedule any tweets on Tweetdeck.
1. Once a tweet is scheduled, the tweet will be moved to the "Scheduled" tab of the spreadsheet.

#### Dribbble

GitLab has a Dribbble team account where you can add work in progress, coming soon, and recently released work.

* If a Dribbble post has a corresponding open issue, link to the issue so designers can contribute on GitLab.
* Add the Dribbble post to our [UX Design Twitter spreadsheet](https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit), along with a link to the corresponding open issue, if applicable.
* If you’re not a member of the GitLab Dribbble team and would like to be, contact the UX Lead to grant you membership.
* [View the GitLab Dribbble team page](https://dribbble.com/GitLab)

## About our team

This section is inspired by the recent trend of Engineering Manager READMEs. _e.g.,_ [Hackernoon: 12 Manager READMEs (from some of the best cultures in tech)](https://hackernoon.com/12-manager-readmes-from-silicon-valleys-top-tech-companies-26588a660afe). Get to know more about the people on our team!

* [Christie Lenneville](/handbook/engineering/ux/one-pagers/christie-readme/) - UX Director
* [Valerie Karnes](https://gitlab.com/vkarnes/readme) - UX Manager
* [Shane Bouchard](/handbook/engineering/ux/one-pagers/shane-readme/) - UX Manager for Plan, Create and Manage
* [Sarah O'Donnell](/handbook/engineering/ux/one-pagers/sarahod-readme/) - UX Research Manager
* [Jacki Bauer](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md) - UX Manager
* [Aaron K. White](/handbook/engineering/ux/one-pagers/awhite-gl-readme/) - UX Manager, Ops
* [Rayana Verissimo](https://gitlab.com/rverissimo/readme) - Sr. Product Designer
* [Matej Latin](https://gitlab.com/matejlatin/focus) - Sr. Product Designer
* [Kyle Mann](https://gitlab.com/kmann/introspection) - Sr. Product Designer
* [Mike Lewis](https://gitlab.com/mikelewis/about) - Technical Writing Manager
* [Iain Camacho](https://gitlab.com/icamacho/koda/blob/master/README.md) - Sr. Product Designer
* [Jeremy Elder](https://gitlab.com/jeldergl/view/blob/master/README.md) - Sr. Product Designer, Visual Design

[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
[everyone-designer]: https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434
[pajamas]: https://design.gitlab.com
