---
layout: handbook-page-toc
title: "Product Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Product marketing at GitLab

The product marketing team at GitLab is responsible for product positioning,  value messaging, and go-to-market strategy in order to support sales and outbound messaging with analysts, press, and prospects as well as facilitating inbound market data input into the product roadmap. 

There are two equally important sides of product marketing at GitLab. First, is understanding the enterprise development team challenges and communicating how GitLab addresses specific their challenges and [use cases](/handbook/use-cases). Second, is a focus on the value and capabilities of the 10 DevOps stages and categories that GitLab enables.

In general, product marketing
1. Develops value based messaging and positioning to support GitLab sales and marketing
1. Enables sales with content and collateral
1. Supports field and digital campaigns with content, webinars, presentations, and strategic input.

### Use cases  
[Customer 'use cases'](/handbook/use-cases/) are a customer problem or initiative that needs a solution and attracts budget, typically defined In customer terms. In Product Marketing, we build content and messaging that engages prospects who are looking for solutions to specific challenges they face. In product marketing, we:
1. Research and prioritize customer 'use cases'.
1. Lead analyst reports and contribute to analyst research.
1. Define the ['buyer's journey'](/handbook/marketing/product-marketing/usecase-gtm) for a specific use case.
1. Audit our existing content and collateral for each stage of the buyer's journey.
1. Prioritize and refine existing or create new content.
1. Collaborate with the rest of the marketing groups (Marketing Program Management, Content Marketing, Digital Marketing, SDRs, etc.) to promote and measure GTM effectiveness.

### Stages and categories
DevOps stages and categories organize and define how we plan and engineer new GitLab features.   
1. Contribute to the stage and category vision.
1. Lead messaging for release posts and support feature descriptions.
1. Competitive research and comparisons.


The [Product Marketing - Overview Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1074672) is where we plan, manage, and track our work.

### Organization

There are two principles in how the team is organized.
1. Ensuring we have coverage across both UseCases **and** DevOps stages so that we can communicate GitLab value **and** support the product and engineering teams building Gitlab.
2. Establish a structure that provides clarity for responsibilities and opportunities for career growth and development.

![PMM Model](pmm-team-model-oct-2019.png)

In this model, Senior PMMs are responsible for both the collateral and messaging supporting a specific Use Case **and** one or several stages. PMMs support the team with messaging and go to market efforts, leading research, writing, and collateral development.

##### Key links
- [Messaging](/handbook/marketing/product-marketing/messaging/)
- [GitLab positioning](/handbook/positioning-faq/)
- [Hidden IT Groups](/handbook/marketing/product-marketing/it-groups/)
- [Defining GitLab roles and personas](/handbook/marketing/product-marketing/roles-personas/)
- [GitLab tiers](/handbook/marketing/product-marketing/tiers/)
- [Market segmentation - Industry verticals](/handbook/marketing/product-marketing/market-segmentation/)
- [Use Case - Go to market](/handbook/marketing/product-marketing/usecase-gtm)


#### Release vs. launch
A [product release, and a marketing launch are two separate activities](http://www.startuplessonslearned.com/2009/03/dont-launch.html). The canonical example of this is Apple. They launch the iPhone at their yearly event and then release it months later. At GitLab we do it the other way: Release features as soon as they are ready letting customers use them right away, and then, do a marketing launch later when we have market validation.

| Release | Launch |
|-|-|
| PM Led | PMM Led |
| New features can ship with or without marketing support | Launch timing need not be tied to the proximity of when a feature was released |

### Team Structure

### Which product marketing manager should I contact?

- Listed below are areas of responsibility within the product marketing team:
  - [William](/company/team/#thewilliamchia), PMM for [CI/CD](/handbook/product/categories/#cicd-section) & [Ops](/handbook/product/categories/#ops-section) Product Sections
  - [Cindy](/company/team/#cblake2000), PMM for [Secure](/handbook/product/categories/#secure-section) & [Defend](/handbook/product/categories/#defend-section) Product Sections
  - [Traci](/company/team/#tracirobinsonwm), Senior PMM, Regulated Industries
  - [Brian](/company/team/##product-marketing-manager,-monitoring), Product Marketing Manager,
  - [John](/company/team/#j_jeremiah), Manager PMM for [Dev Product Section](/handbook/product/categories/#dev-section)
  - [Ashish](/company/team/#kuthiala), Director PMM
